-- To login to MySQL
mysql -u root -p

-- To show the list of databases
SHOW DATABASES;

-- For creating a new database
CREATE DATABASE enrollment_db;

-- For switching the active database
USE enrollment_db;

-- Creating tables in a database
CREATE TABLE courses (
	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(50) NOT NULL,
	teacher_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_courses_teacher_id
		FOREIGN KEY (teacher_id) REFERENCES teachers(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE students (
	id INT NOT NULL AUTO_INCREMENT,
	student_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE teachers (
	id INT NOT NULL AUTO_INCREMENT,
	teacher_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- For creating tables with a foreign key, we use the CONSTRAINT keyword where we can set the local property which serves as the foreign key that is connected to a different table. Within the CONSTRAINT keyword, we also declare the behavior of the property when it is updated or deleted.
-- CASCADE means the foreign key property will update if the id of the table that it is referencing is also updated.
-- RESTRICT means the opposite of CASCADE wherein it won't update or do anything to the foreign key property once the table property that it is referencing is deleted.
CREATE TABLE student_courses (
	id INT NOT NULL AUTO_INCREMENT,
	course_id INT NOT NULL,
	student_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_student_courses_course_id
		FOREIGN KEY (course_id) REFERENCES courses(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_student_courses_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);
